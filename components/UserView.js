import React, {Component} from "react";
import {StyleSheet, Text, View, Share, Button} from "react-native";

export default class UserView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        }
    }

    shareMessage = () => {
        Share.share({message: this.getUserAsString()})
            .then(result => console.log(result))
            .catch(errorMsg => console.log(errorMsg));
    };

    getUserAsString = () => {
        const {user} = this.state;
        return "Name: " + user.name +"\n E - mail: " + user.email + "\n Phone: " + user.phone + "\n";
    };

    render() {
        return (
            <View>
                <Button
                    onPress={() => this.shareMessage() }
                    title="Share"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
                <Text style={styles.user}>
                    {this.getUserAsString()}
                </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    user: {
        textAlign: 'center',
        margin: 5,
    },
});