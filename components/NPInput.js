import React, { Component,  } from 'react';
import { AppRegistry, TextInput, Button, View } from 'react-native';


export default class NPInput extends Component{
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            onSubmit: this.props.onSubmit,
            placeholder: this.props.placeholder
        }
    }

    render () {
        const {onSubmit, text, placeholder} = this.state;
        return (
        <View>
            <TextInput
                style={{width: 200, borderColor: 'gray', borderWidth: 1}}
                onChangeText={(text) => this.setState({text: text})}
                placeholder={placeholder}
                value={text}
            />
            <Button
                onPress={() => onSubmit(text)}
                title="Check user"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
            />
        </View>
        )
    }

}