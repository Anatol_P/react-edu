/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    ActivityIndicator
} from 'react-native';
import NPInput from './components/NPInput';
import UserView from "./components/UserView";

export default class App extends Component<{}> {
    state = {
        user: null,
        error: null,
        wait: false,
    };

    renderError = () => {
        const {error} = this.state;
        return error ? (<Text style={styles.welcome}>{error}</Text>) : null
    };

    renderSpinner = () => {
        const {wait} = this.state;
        return wait ? (<ActivityIndicator size="large" color="#0000ff" />) : null;
    };

    sendText = text => {
        this.setState({wait: true});
        fetch(url + '/react-api/user/' + text)
            .then((res) => res.text())
            .then((user) => user.length ? this.setState({user: JSON.parse(user), wait: false}) : this.setState({error: 'User ' + text + ' not found', wait: false}))
    };

    render() {
        const {user} = this.state;
        return (
            <View style={styles.container}>
                {user ?
                    (
                        <View>
                            <UserView user={user}/>
                            <Button
                                onPress={() => this.setState({user: null, error: null, wait: false})}
                                title="Search again"
                                color="#841584"
                                style={{justifyContent: 'flex-end'}}
                                accessibilityLabel="this purple button"
                            />
                        </View>
                    )
                    : (<View>
                            <NPInput
                                placeholder={'Enter user'}
                                onSubmit={this.sendText}/>
                            {this.renderError()}
                            {this.renderSpinner()}
                        </View>
                    )
                }
            </View>
        )
    }
}

const url = 'https://b267ec1e.ngrok.io';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});